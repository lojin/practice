== Работа с объектом namespace

. Получитие список всех namespace в вашем кластере
. Создайте `namespace` с именем `cnad`
.. Использую императивный подход (cli)
.. Используя декларативный подход (yaml + cli) используйте заготовку файла `namespace.yaml` в текущем каталоге.
. Запустите контейнер из задания `Задание 2. Создание Docker образов (images)` в `namespace` `cnad`.
+
.команда для запуска контейнера
[source, bash]
----
kubectl run exercise-5 --image registry.gitlab.com/cloud-native-development1/module-1/practice/docker/task2:<tag-name> <1>
----
+
<1> [tag-name] используйте свой образ созданный во 2 задании (a-nozdryn-platnitski).

. Получитие список всех namespace в вашем кластере. Убедитесь что `namespace` `cnad` есть в списке.
. Удалите `namespace` `cnad`
. Получитие список всех namespace в вашем кластере. Убедитесь что `namespace` `cnad` отсутствует в списке.


https://kubernetes.io/ru/docs/concepts/overview/working-with-objects/namespaces/[Документация по namespace.]

https://kubernetes.io/docs/reference/kubectl/cheatsheet/[kubectl cheat sheet.]
